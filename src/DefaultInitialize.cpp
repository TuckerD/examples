/** ////////////////////////////////////////////////////////////////////////////
* @file DefaultInitialize.cpp
*
* @copyright 2018 Tucker Downs. MIT License
//////////////////////////////////////////////////////////////////////////////*/

#include <string>
#include <cstdint>
#include <functional>
#include <array>
#include <iostream>
#include <chrono>

struct MyStruct {
    uint64_t value;
    std::array<int32_t, 50000> array;
};

int get(MyStruct& ioVar, std::function<int(int)> f, int var){
    ioVar.value = f(var);
    int i = 0;
    for(auto& value : ioVar.array) {
        ++i;
        value = f(var * var * i) + f(var * var * i) / 7;
    }
    ioVar.array[20] = 24;
    return 0;
}

int main(int argc, char* argv[]) {
    using namespace std;
    using namespace std::chrono;

    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    MyStruct rm{};
    high_resolution_clock::time_point t2 = high_resolution_clock::now();

    auto d1 = duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    cout << "memset was \t" << d1 << " microsecconds\n";

    high_resolution_clock::time_point t3 = high_resolution_clock::now();
    get(rm, [argc](int x) {return x * argc;}, argc);
    high_resolution_clock::time_point t4 = high_resolution_clock::now();

    auto d2 = duration_cast<std::chrono::microseconds>( t4 - t3 ).count();
    cout << "func was \t" << d2 << " microsecconds\n";

    return 0;
}
