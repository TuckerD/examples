#include <iostream>

struct BigStruct {
    std::string myString;

    void foo(std::string&& s) {
        this->myString = s;  // Does not move!!!! s is a "named value"
        // this->myString = std::move(s); // Actually performs a move
    }

    void bar(const std::string& s) {
        this->myString = std::move(s); // Casts to const std::string &&, which calls copy assignment (cannot move from const)
    }

    void baz(std::string& s) {
        this->myString = std::move(s); // Casts to std::string &&, which calls move assignment (Move memory from passed in reference)
    }
};

int main(int argc, char* argv[]) {
    BigStruct someObj{};
    std::string aStr{"Hello"};

    someObj.foo(std::move(aStr));

    // print aStr to see if it has actually been moved from
    std::cout << aStr << "\n";
}
