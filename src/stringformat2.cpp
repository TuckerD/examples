#include <stdarg.h>
#include <cstdio>

#include <string>
#include <algorithm>
#include <chrono>
#include <iostream>

typedef unsigned long ulong;

/******************************************************************************/
std::string vssprintf(const char* fmt, va_list ap, ulong maxsz = 10000u) {
    char buf[1024];
	int len = vsnprintf(buf, 1024, fmt, ap);
	if (len < 0)
		return "<vsnprintf_error>";
	if ((ulong)len > maxsz)
		len = maxsz;

	std::string s(len+1, 0);
	vsnprintf(s.data(), len+1, fmt, ap);
	s.resize(len);
	return s;
}
/******************************************************************************/
std::string ssprintf(const char* fmt, ...) {
	va_list ap;
	va_start(ap, fmt);
	std::string s(vssprintf(fmt, ap));
	va_end(ap);
	return s;
}
/******************************************************************************/

std::string ssprintf2(const char* fmt, ...) {
	va_list ap;
	va_start(ap, fmt);

    const size_t BUF_SIZE = 10000;
    static char buf[BUF_SIZE];
    size_t n = snprintf(buf, BUF_SIZE, fmt, ap);
    std::string s{buf, n};

	va_end(ap);
	return s;
}

int main(int argc, char* argv[]) {
    const int NUM_RUNS = 10000000;

    std::cout << "Running " << NUM_RUNS << " iterations.\n";

    auto running_time_str = 0;
    auto running_time_ctos = 0;
    auto running_time_char = 0;

    auto format = "The numbeadfdasfdafadfadfadfadfadsdfdasfdasfr is \t %d. Sfadsfadsfadsfadsfadsfadsfadsfadsfadsfadsfdaso%dme moasdfadsfaadsfadsfafdwqtgrbdsfadsfadsre mo%dradsfadsfadsfadsfadsfadsfadsfadsfadsfadsfadsfadsfe t%dexadsfadsfadsfdsafsdfasfsdafdafdafdasfdfadsft\n";
    for(int i = 0; i < NUM_RUNS; i++) {
        {
            auto t1 = std::chrono::high_resolution_clock::now();
            std::string s1 = ssprintf(format, argc, argc, argc, argc);
            auto t2 = std::chrono::high_resolution_clock::now();
            running_time_str += std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
        }
        {
            auto t1 = std::chrono::high_resolution_clock::now();
            char buf[256];
            size_t n = snprintf(buf, 256, format, argc, argc, argc, argc);
            std::string s2{buf, n};
            auto t2 = std::chrono::high_resolution_clock::now();
            running_time_ctos += std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
        }
        {
            auto t1 = std::chrono::high_resolution_clock::now();
            std::string s1 = ssprintf2(format, argc, argc, argc, argc);
            auto t2 = std::chrono::high_resolution_clock::now();
            running_time_char += std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
        }
    }

    std::cout << "ssprintf                   was \t" << running_time_str << " microsecconds\n";
    std::cout << "char buffer w/ copy to str was \t" << running_time_ctos << " microsecconds\n";
    std::cout << "ssprintf2                  was \t" << running_time_char << " microsecconds\n";

    return 0;
}
